#include <BombUtils.h>

/* randomly adds characters of 'letters' string to serial, ending with an int */
String generateSerial(){
  String letters = F("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
  String serial = "";
  for(int i = 0; i < 5; i++){ // choose 4 random characters/numbers
    serial.concat(letters.charAt(random(0, 36)));
  }
  serial.concat(random(0, 10)); // add digit at last position

  return serial;
}

/* returns true if given string contains at least one vowel (a,e,i,o,u) */
static bool containsVowel(String string){
  string.toLowerCase();

  if(string.indexOf('a') != -1){
    return true;
  }
  else if(string.indexOf('e') != -1){
   return true;
  }
  else if(string.indexOf('i') != -1){
    return true;
  }
  else if(string.indexOf('o') != -1){
    return true;
  }
  else if(string.indexOf('u') != -1){
    return true;
  }

  return false;
}
