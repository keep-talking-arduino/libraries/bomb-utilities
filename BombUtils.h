#ifndef BombUtils_H
#define BombUtils_H

#include <Arduino.h>

String generateSerial();
bool containsVowel(String string);

#endif
